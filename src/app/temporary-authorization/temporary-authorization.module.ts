import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TemporaryAuthorizationPageRoutingModule } from './temporary-authorization-routing.module';

import { TemporaryAuthorizationPage } from './temporary-authorization.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TemporaryAuthorizationPageRoutingModule
  ],
  declarations: [TemporaryAuthorizationPage]
})
export class TemporaryAuthorizationPageModule {}
