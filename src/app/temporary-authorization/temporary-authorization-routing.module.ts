import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TemporaryAuthorizationPage } from './temporary-authorization.page';

const routes: Routes = [
  {
    path: '',
    component: TemporaryAuthorizationPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TemporaryAuthorizationPageRoutingModule {}
