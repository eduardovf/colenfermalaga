import { SpecialtiesListPage } from './specialties-list.page';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SpecialtiesEditPage } from './edit/specialties-edit.page';

const routes: Routes = [
  { path: '', component: SpecialtiesListPage },
  { path: 'new', component: SpecialtiesEditPage },
  { path: 'edit', component: SpecialtiesEditPage },
  { path: 'edit/:id', component: SpecialtiesEditPage }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SpecialtiesRoutingModule {}
