import { AlertController, LoadingController, ToastController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-specialties-list',
  templateUrl: './specialties-list.page.html',
  styleUrls: ['./specialties-list.page.scss'],
})
export class SpecialtiesListPage implements OnInit {

  isLoading: boolean;
  specialties: any;

  constructor(
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private alertController: AlertController
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.getData();
  }

  getData() {
    this.isLoading = true;
    this.loadingCtrl
      .create({keyboardClose: true, message: 'Cargando...'})
      .then(loadingEl => {
        loadingEl.present();
        setTimeout(() => {
          this.specialties = [
            {
              id: 1,
              specialty: 'Asistencia obstétrica (matrona)',
              date: '2020-02-01',
              title: 'diplomado/a',
              via: 'EIR',
              pending: false
            },
            {
              id: 2,
              specialty: 'Asistencia obstétrica (matrona)',
              date: '2020-02-01',
              title: 'diplomado/a',
              via: 'EIR',
              pending: true
            },
          ];
          this.isLoading = false;
          loadingEl.dismiss();
        }, 1500);
      });
  }

  deleteSpecialty(specialtyId) {
    this.alertController
      .create({
        header: '¿Estás seguro?',
        message: 'Has solicitado eliminar un elemento. Confirma que  es eso lo que quieres hacer.',
        buttons: [
          {
            text: 'Cancelar',
            role: 'cancel',
            cssClass: 'secondary'
          },
          {
            text: 'OK',
            cssClass: 'primary',
            handler: () => {
              this.loadingCtrl
                .create({keyboardClose: true, message: 'Enviando...'})
                .then(loadingEl => {
                  loadingEl.present();
                  setTimeout(() => {
                    this.toastCtrl
                      .create({
                        message: '<b>La solicitud ha sido enviada.</b><br>Recuerde que estos cambios serán revisados por el Colegio antes de hacerse efectivos',
                        duration: 3000,
                        cssClass: 'my-success-toast',
                        color: 'success'
                      })
                      .then(toastEl => {
                        toastEl.present();
                      });
                    loadingEl.dismiss();
                    this.getData();
                  }, 1500);
                });
            }
          }
        ]
      })
      .then(alertEl => {
        alertEl.present();
      });
  }
}
