import { SpecialtiesListPage } from './specialties-list.page';
import { SpecialtiesRoutingModule } from './specialties-routing.module';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SpecialtiesEditPage } from './edit/specialties-edit.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SpecialtiesRoutingModule
  ],
  declarations: [
    SpecialtiesListPage,
    SpecialtiesEditPage,
  ],
})
export class SpecialtiesModule { }
