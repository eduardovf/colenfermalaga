import { LoadingController, ToastController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-specialties-edit',
  templateUrl: './specialties-edit.page.html',
  styleUrls: ['./specialties-edit.page.scss'],
})
export class SpecialtiesEditPage implements OnInit {

  specialtyId: any;
  isLoading: boolean;
  specialty: any;

  constructor(
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.paramMap.subscribe(paramMap => {
      this.specialtyId = paramMap.has('id') || null;
      if(this.specialtyId) {
        this.getData(this.specialtyId);
      }
    });
  }

  getData(specialtyId) {
    if(specialtyId) {
      this.isLoading = true;
      this.loadingCtrl
        .create({keyboardClose: true, message: 'Cargando...'})
        .then(loadingEl => {
          loadingEl.present();
          setTimeout(() => {
            this.specialty = {
              id: 1,
              specialty: 'Asistencia obstétrica (matrona)',
              date: '2020-02-01',
              title: 'Diplomado/a',
              via: 'EIR'
            };
            this.isLoading = false;
            loadingEl.dismiss();
          }, 1500);
        });
      }
  }

  submit() {
    this.loadingCtrl
      .create({keyboardClose: true, message: 'Enviando...'})
      .then(loadingEl => {
        loadingEl.present();
        setTimeout(() => {
          this.toastCtrl
            .create({
              message: '<b>La solicitud ha sido enviada.</b><br>Recuerde que estos cambios serán revisados por el Colegio antes de hacerse efectivos',
              duration: 3000,
              cssClass: 'my-success-toast',
              color: 'success'
            })
            .then(toastEl => {
              toastEl.present();
            });
          loadingEl.dismiss();
        }, 1500);
      });
  }
}
