import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WebConfigurationPage } from './web-configuration.page';

const routes: Routes = [
  {
    path: '',
    component: WebConfigurationPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WebConfigurationPageRoutingModule {}
