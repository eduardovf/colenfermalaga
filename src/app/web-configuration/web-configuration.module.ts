import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { WebConfigurationPageRoutingModule } from './web-configuration-routing.module';

import { WebConfigurationPage } from './web-configuration.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    WebConfigurationPageRoutingModule
  ],
  declarations: [WebConfigurationPage]
})
export class WebConfigurationPageModule {}
