import { LoadingController, ToastController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-academics-edit',
  templateUrl: './academics-edit.page.html',
  styleUrls: ['./academics-edit.page.scss'],
})
export class AcademicsEditPage implements OnInit {

  academicId: any;
  isLoading: boolean;
  academic: any;

  constructor(
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.paramMap.subscribe(paramMap => {
      this.academicId = paramMap.has('id') || null;
      if(this.academicId) {
        this.getData(this.academicId);
      }
    });
  }

  getData(academicId) {
    if(academicId) {
      this.isLoading = true;
      this.loadingCtrl
        .create({keyboardClose: true, message: 'Cargando...'})
        .then(loadingEl => {
          loadingEl.present();
          setTimeout(() => {
            this.academic = {
              id: 1,
              center: 'Nombre del centro',
              date: '2020-02-01',
              name: 'Graduado en enfermería',
              title: 'Grado',
              area: 'Área de estudio',
              main: true,
              pending: false
            };
            this.isLoading = false;
            loadingEl.dismiss();
          }, 1500);
        });
      }
  }

  submit() {
    this.loadingCtrl
      .create({keyboardClose: true, message: 'Enviando...'})
      .then(loadingEl => {
        loadingEl.present();
        setTimeout(() => {
          this.toastCtrl
            .create({
              message: '<b>La solicitud ha sido enviada.</b><br>Recuerde que estos cambios serán revisados por el Colegio antes de hacerse efectivos',
              duration: 3000,
              cssClass: 'my-success-toast',
              color: 'success'
            })
            .then(toastEl => {
              toastEl.present();
            });
          loadingEl.dismiss();
        }, 1500);
      });
  }
}
