import { AcademicsEditPage } from './edit/academics-edit.page';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AcademicsListPage } from './academics-list.page';

const routes: Routes = [
  { path: '', component: AcademicsListPage },
  { path: 'new', component: AcademicsEditPage },
  { path: 'edit/:id', component: AcademicsEditPage }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AcademicsRoutingModule {}
