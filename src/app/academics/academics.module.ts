import { AcademicsEditPage } from './edit/academics-edit.page';
import { AcademicsRoutingModule } from './academics-routing.module';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AcademicsListPage } from './academics-list.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AcademicsRoutingModule
  ],
  declarations: [
    AcademicsListPage,
    AcademicsEditPage,
  ],
})
export class AcademicsModule { }
