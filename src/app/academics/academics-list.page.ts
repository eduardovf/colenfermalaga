import { AlertController, LoadingController, ToastController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-academics-list',
  templateUrl: './academics-list.page.html',
  styleUrls: ['./academics-list.page.scss'],
})
export class AcademicsListPage implements OnInit {

  isLoading: boolean;
  academics: any;

  constructor(
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private alertController: AlertController
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.getData();
  }

  getData() {
    this.isLoading = true;
    this.loadingCtrl
      .create({keyboardClose: true, message: 'Cargando...'})
      .then(loadingEl => {
        loadingEl.present();
        setTimeout(() => {
          this.academics = [
            {
              id: 1,
              center: 'Nombre del centro',
              date: '2020-02-01',
              name: 'Graduado en enfermería',
              title: 'Grado',
              area: 'Área de estudio',
              main: true,
              pending: false
            },
            {
              id: 2,
              center: 'Nombre del centro 02',
              date: '2020-02-01',
              name: 'Graduado en enfermería',
              title: 'Grado',
              area: 'Grado',
              main: false,
              pending: true
            },
            {
              id: 3,
              center: 'Nombre del centro 03',
              date: '2020-02-01',
              name: 'Graduado en enfermería',
              title: 'Grado',
              area: 'Área de estudio',
              main: false,
              pending: false
            },
          ];
          this.isLoading = false;
          loadingEl.dismiss();
        }, 1500);
      });
  }

  deleteAcademic(academicId) {
    this.alertController
      .create({
        header: '¿Estás seguro?',
        message: 'Has solicitado eliminar un elemento. Confirma que  es eso lo que quieres hacer.',
        buttons: [
          {
            text: 'Cancelar',
            role: 'cancel',
            cssClass: 'secondary'
          },
          {
            text: 'OK',
            cssClass: 'primary',
            handler: () => {
              this.loadingCtrl
                .create({keyboardClose: true, message: 'Enviando...'})
                .then(loadingEl => {
                  loadingEl.present();
                  setTimeout(() => {
                    this.toastCtrl
                      .create({
                        message: '<b>La solicitud ha sido enviada.</b><br>Recuerde que estos cambios serán revisados por el Colegio antes de hacerse efectivos',
                        duration: 3000,
                        cssClass: 'my-success-toast',
                        color: 'success'
                      })
                      .then(toastEl => {
                        toastEl.present();
                      });
                    loadingEl.dismiss();
                    this.getData();
                  }, 1500);
                });
            }
          }
        ]
      })
      .then(alertEl => {
        alertEl.present();
      });
  }
}
