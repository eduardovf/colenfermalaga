import { Component, OnInit } from '@angular/core';
import { LoadingController, ToastController } from '@ionic/angular';
import { Chooser } from '@awesome-cordova-plugins/chooser/ngx';

@Component({
  selector: 'app-social-benefit',
  templateUrl: './social-benefit.page.html',
  styleUrls: ['./social-benefit.page.scss'],
  providers: [ Chooser ]
})
export class SocialBenefitPage implements OnInit {

  isLoading: boolean;
  collegiate: any;

  documents = [
    { id: 1, name: 'Documento_01.pdf' },
    { id: 2, name: 'Documento_02.pdf' },
    { id: 3, name: 'Documento_03.pdf' }
  ]

  constructor(
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private chooser: Chooser) { }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.isLoading = true;
    this.loadingCtrl
      .create({keyboardClose: true, message: 'Cargando...'})
      .then(loadingEl => {
        loadingEl.present();
        setTimeout(() => {
          this.collegiate = {
            phone: '555123456',
            email: 'michael.terence.smith@midominio.com'
          };
          this.isLoading = false;
          loadingEl.dismiss();
        }, 1500);
      });
  }

  submit() {
    this.loadingCtrl
      .create({keyboardClose: true, message: 'Enviando...'})
      .then(loadingEl => {
        loadingEl.present();
        setTimeout(() => {
          this.toastCtrl
            .create({
              message: '<b>La solicitud ha sido enviada.</b><br>Recuerde que estos cambios serán revisados por el Colegio antes de hacerse efectivos',
              duration: 3000,
              cssClass: 'my-success-toast',
              color: 'success'
            })
            .then(toastEl => {
              toastEl.present();
            });
          loadingEl.dismiss();
        }, 1500);
      });
  }

  upload() {
    this.chooser.getFile()
    .then(file => {
      console.log(file ? file.name : 'canceled');
    })
    .catch((error: any) => console.error(error));
  }

  deleteFile(id) {
    console.log("Borrar documento: " + this.documents.find( (d)=>d.id===id).name);
  }
}
