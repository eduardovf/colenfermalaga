import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SocialBenefitPage } from './social-benefit.page';

const routes: Routes = [
  {
    path: '',
    component: SocialBenefitPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SocialBenefitPageRoutingModule {}
