import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SocialBenefitPageRoutingModule } from './social-benefit-routing.module';

import { SocialBenefitPage } from './social-benefit.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SocialBenefitPageRoutingModule
  ],
  declarations: [SocialBenefitPage]
})
export class SocialBenefitPageModule {}
