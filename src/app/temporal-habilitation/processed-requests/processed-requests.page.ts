import { LoadingController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-processed-requests',
  templateUrl: './processed-requests.page.html',
  styleUrls: ['./processed-requests.page.scss'],
})
export class ProcessedRequestsPage implements OnInit {

  isLoading: boolean;
  items: any;

  constructor(private loadingCtrl: LoadingController) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.getData();
  }

  getData() {
    this.isLoading = true;
    this.loadingCtrl
      .create({keyboardClose: true, message: 'Cargando...'})
      .then(loadingEl => {
        loadingEl.present();
        setTimeout(() => {
          this.items = [
            {
              id: 1,
              center: 'Nombre del centro',
              province: 'Almería',
              dateRequest: '2020-02-01',
              dateStart: '2020-02-01',
              dateEnd: '2020-02-01',
              status: 0,
              refusal: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce mattis dui nec augue auctor, et blandit ipsum viverra.'
            },
            {
              id: 2,
              center: 'Nombre del centro 2',
              province: 'Almería',
              dateRequest: '2020-02-01',
              dateStart: '2020-02-01',
              dateEnd: '2020-02-01',
              status: 1
            },
          ];
          this.isLoading = false;
          loadingEl.dismiss();
        }, 1500);
      });
  }
}
