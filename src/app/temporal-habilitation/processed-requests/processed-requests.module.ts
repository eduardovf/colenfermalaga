import { ProcessedRequestsPage } from './processed-requests.page';
import { ProcessedRequestsPageRoutingModule } from './processed-requests-routing.module';
import { SharedModule } from './../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    ProcessedRequestsPageRoutingModule
  ],
  declarations: [ProcessedRequestsPage],
})
export class ProcessedRequestsPageModule {}
