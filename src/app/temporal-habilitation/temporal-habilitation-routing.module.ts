import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TemporalHabilitationPage } from './temporal-habilitation.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TemporalHabilitationPage,
    children: [
      {
        path: 'awaiting',
        loadChildren: () => import('./awaiting-requests/awaiting-requests.module').then( m => m.AwaitingRequestsPageModule)
      },
      {
        path: 'processed',
        loadChildren: () => import('./processed-requests/processed-requests.module').then( m => m.ProcessedRequestsPageModule)
      },
      {
        path: '',
        redirectTo: '/tabs/awaiting',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/temporal-habilitation/tabs/awaiting',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TemporalHabilitationPageRoutingModule {}
