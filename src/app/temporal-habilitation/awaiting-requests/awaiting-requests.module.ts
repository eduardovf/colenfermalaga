import { AwaitingRequestsPageRoutingModule } from './awaiting-requests-routing.module';
import { AwaitingRequestsPage } from './awaiting-requests.page';
import { SharedModule } from './../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    AwaitingRequestsPageRoutingModule
  ],
  declarations: [AwaitingRequestsPage],
})
export class AwaitingRequestsPageModule {}
