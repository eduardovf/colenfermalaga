import { LoadingController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-awaiting-requests',
  templateUrl: './awaiting-requests.page.html',
  styleUrls: ['./awaiting-requests.page.scss'],
})
export class AwaitingRequestsPage implements OnInit {

  isLoading: boolean;
  items: any;

  constructor(private loadingCtrl: LoadingController) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.getData();
  }

  getData() {
    this.isLoading = true;
    this.loadingCtrl
      .create({keyboardClose: true, message: 'Cargando...'})
      .then(loadingEl => {
        loadingEl.present();
        setTimeout(() => {
          this.items = [
            {
              id: 1,
              center: 'Nombre del centro',
              province: 'Almería',
              dateRequest: '2020-02-01',
              dateStart: '2020-02-01',
              dateEnd: '2020-02-01'
            },
            {
              id: 2,
              center: 'Nombre del centro 2',
              province: 'Sevilla',
              dateRequest: '2020-02-01',
              dateStart: '2020-02-01',
              dateEnd: '2020-02-01'
            },
          ];
          this.isLoading = false;
          loadingEl.dismiss();
        }, 1500);
      });
  }
}
