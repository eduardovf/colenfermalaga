import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TemporalHabilitationPageRoutingModule } from './temporal-habilitation-routing.module';

import { TemporalHabilitationPage } from './temporal-habilitation.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TemporalHabilitationPageRoutingModule
  ],
  declarations: [TemporalHabilitationPage]
})
export class TemporalHabilitationPageModule {}
