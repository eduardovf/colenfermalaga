import { BusinessEditPage } from './edit/business-edit.page';
import { BusinessListPage } from './business-list.page';
import { BusinessRoutingModule } from './business-routing.module';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BusinessRoutingModule
  ],
  declarations: [
    BusinessListPage,
    BusinessEditPage,
  ]
})
export class BusinessModule { }
