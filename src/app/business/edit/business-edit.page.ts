import { LoadingController, ToastController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-business-edit',
  templateUrl: './business-edit.page.html',
  styleUrls: ['./business-edit.page.scss'],
})
export class BusinessEditPage implements OnInit {

  businessId: any;
  isLoading: boolean;
  business: any;

  constructor(
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.paramMap.subscribe(paramMap => {
      this.businessId = paramMap.has('id') || null;
      if(this.businessId) {
        this.getData(this.businessId);
      }
    });
  }

  getData(addressId) {
    if(addressId) {
      this.isLoading = true;
      this.loadingCtrl
        .create({keyboardClose: true, message: 'Cargando...'})
        .then(loadingEl => {
          loadingEl.present();
          setTimeout(() => {
            this.business = {
              id: 1,
              title: 'Empresa 01',
              type: 'Pública',
              address: 'CALLE DE MARIE CURIE, 7',
              address2: 'Se entra por puerta secreta',
              cp: '29590',
              localty: 'Campanillas',
              province: 'Málaga',
              movil: '61234567',
              phone: '555111222',
              email: 'email.empresa01@gmail.com',
              web: 'https://www.empresa_01.com',
              category: 'Técnico de emergencias',
              function: 'Responder a emergencias',
              pending: false
              };
            this.isLoading = false;
            loadingEl.dismiss();
          }, 1500);
        });
      }
  }

  submit() {
    this.loadingCtrl
      .create({keyboardClose: true, message: 'Enviando...'})
      .then(loadingEl => {
        loadingEl.present();
        setTimeout(() => {
          this.toastCtrl
            .create({
              message: '<b>La solicitud ha sido enviada.</b><br>Recuerde que estos cambios serán revisados por el Colegio antes de hacerse efectivos',
              duration: 3000,
              cssClass: 'my-success-toast',
              color: 'success'
            })
            .then(toastEl => {
              toastEl.present();
            });
          loadingEl.dismiss();
        }, 1500);
      });
  }
}
