import { BusinessEditPage } from './edit/business-edit.page';
import { BusinessListPage } from './business-list.page';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', component: BusinessListPage },
  { path: 'new', component: BusinessEditPage },
  { path: 'edit', component: BusinessEditPage },
  { path: 'edit/:id', component: BusinessEditPage }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BusinessRoutingModule {}
