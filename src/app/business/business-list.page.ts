import { AlertController, LoadingController, ToastController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-business-list',
  templateUrl: './business-list.page.html',
  styleUrls: ['./business-list.page.scss'],
})
export class BusinessListPage implements OnInit {

  isLoading: boolean;
  business: any;

  constructor(
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private alertController: AlertController
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.getData();
  }

  getData() {
    this.isLoading = true;
    this.loadingCtrl
      .create({keyboardClose: true, message: 'Cargando...'})
      .then(loadingEl => {
        loadingEl.present();
        setTimeout(() => {
          this.business = [
            {
              id: 1,
              title: 'Empresa 01',
              type: 'Pública',
              address: 'CALLE DE MARIE CURIE, 7',
              address2: 'Se entra por puerta secreta',
              cp: '29590',
              localty: 'Campanillas',
              province: 'Málaga',
              movil: '61234567',
              phone: '555111222',
              email: 'email.empresa01@gmail.com',
              web: 'https://www.empresa_01.com',
              category: 'Técnico de emergencias',
              function: 'Responder a emergencias',
              pending: false
            },
            {
              id: 2,
              title: 'Empresa 02',
              type: 'Privada',
              address: 'CALLE DE MARIE CURIE, 8',
              address2: 'Se entra por puerta secreta',
              cp: '29590',
              localty: 'Campanillas',
              province: 'Málaga',
              movil: '666555444',
              phone: '555111223',
              email: '',
              web: '',
              category: '',
              function: '',
              pending: true
            }
          ];
          this.isLoading = false;
          loadingEl.dismiss();
        }, 1500);
      });
  }

  deleteBusiness(businessId) {
    this.alertController
      .create({
        header: '¿Estás seguro?',
        message: 'Has solicitado eliminar un elemento. Confirma que  es eso lo que quieres hacer.',
        buttons: [
          {
            text: 'Cancelar',
            role: 'cancel',
            cssClass: 'secondary'
          },
          {
            text: 'OK',
            cssClass: 'primary',
            handler: () => {
              this.loadingCtrl
                .create({keyboardClose: true, message: 'Enviando...'})
                .then(loadingEl => {
                  loadingEl.present();
                  setTimeout(() => {
                    this.toastCtrl
                      .create({
                        message: '<b>La solicitud ha sido enviada.</b><br>Recuerde que estos cambios serán revisados por el Colegio antes de hacerse efectivos',
                        duration: 3000,
                        cssClass: 'my-success-toast',
                        color: 'success'
                      })
                      .then(toastEl => {
                        toastEl.present();
                      });
                    loadingEl.dismiss();
                    this.getData();
                  }, 1500);
                });
            }
          }
        ]
      })
      .then(alertEl => {
        alertEl.present();
      });
  }
}
