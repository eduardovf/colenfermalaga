import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DismissalPage } from './dismissal.page';

const routes: Routes = [
  {
    path: '',
    component: DismissalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DismissalPageRoutingModule {}
