import { Component, OnInit } from '@angular/core';
import { LoadingController, ToastController } from '@ionic/angular';
import { Chooser } from '@awesome-cordova-plugins/chooser/ngx';

@Component({
  selector: 'app-dismissal',
  templateUrl: './dismissal.page.html',
  styleUrls: ['./dismissal.page.scss'],
  providers: [ Chooser ]
})
export class DismissalPage implements OnInit {

  constructor(
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private chooser: Chooser) { }

  ngOnInit() {
  }

  submit() {
    this.loadingCtrl
      .create({keyboardClose: true, message: 'Enviando...'})
      .then(loadingEl => {
        loadingEl.present();
        setTimeout(() => {
          this.toastCtrl
            .create({
              message: '<b>La solicitud ha sido enviada.</b><br>Recuerde que estos cambios serán revisados por el Colegio antes de hacerse efectivos',
              duration: 3000,
              cssClass: 'my-success-toast',
              color: 'success'
            })
            .then(toastEl => {
              toastEl.present();
            });
          loadingEl.dismiss();
        }, 1500);
      });
  }

  upload() {
    this.chooser.getFile()
    .then(file => {
      console.log(file ? file.name : 'canceled');
    })
    .catch((error: any) => console.error(error));
  }
}
