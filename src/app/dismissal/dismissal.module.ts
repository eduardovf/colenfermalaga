import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DismissalPageRoutingModule } from './dismissal-routing.module';

import { DismissalPage } from './dismissal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DismissalPageRoutingModule
  ],
  declarations: [DismissalPage]
})
export class DismissalPageModule {}
