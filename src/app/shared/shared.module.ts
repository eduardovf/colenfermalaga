import { TruncatePipe } from './pipes/truncate.pipe';
import { ImagePickerComponent } from './pickers/image-picker/image-picker.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';

@NgModule({
  declarations: [
    ImagePickerComponent,
    TruncatePipe
  ],
  imports: [CommonModule, IonicModule],
  exports: [
    ImagePickerComponent,
    TruncatePipe
  ],
})
export class SharedModule {}
