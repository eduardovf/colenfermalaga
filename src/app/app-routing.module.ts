import { LoginGuard } from './login/login.guard';
import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'home',
    canLoad: [LoginGuard],
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'personal-data',
    loadChildren: () => import('./personal-data/personal-data.module').then( m => m.PersonalDataPageModule)
  },
  {
    path: 'web-configuration',
    loadChildren: () => import('./web-configuration/web-configuration.module').then( m => m.WebConfigurationPageModule)
  },
  {
    path: 'photo-card',
    loadChildren: () => import('./photo-card/photo-card.module').then( m => m.PhotoCardPageModule)
  },
  {
    path: 'addresses',
    loadChildren: () => import('./addresses/addresses.module').then( m => m.AddressesPageModule)
  },
  {
    path: 'professional-data',
    loadChildren: () => import('./professional-data/professional-data.module').then( m => m.ProfessionalDataPageModule)
  },
  {
    path: 'business',
    loadChildren: () => import('./business/business.module').then( m => m.BusinessModule)
  },
  {
    path: 'curriculums',
    loadChildren: () => import('./curriculums/curriculums.module').then( m => m.CurriculumsModule)
  },
  {
    path: 'academics',
    loadChildren: () => import('./academics/academics.module').then( m => m.AcademicsModule)
  },
  {
    path: 'specialties',
    loadChildren: () => import('./specialties/specialties.module').then( m => m.SpecialtiesModule)
  },
  {
    path: 'languages',
    loadChildren: () => import('./languages/languages.module').then( m => m.LanguagesModule)
  },
  {
    path: 'inscriptions',
    loadChildren: () => import('./inscriptions/inscriptions.module').then( m => m.InscriptionsPageModule)
  },
  {
    path: 'certificates',
    loadChildren: () => import('./certificates/certificates.module').then( m => m.CertificatesPageModule)
  },
  {
    path: 'temporal-habilitation',
    loadChildren: () => import('./temporal-habilitation/temporal-habilitation.module').then( m => m.TemporalHabilitationPageModule)
  },
  {
    path: 'bills',
    loadChildren: () => import('./bills/bills.module').then( m => m.BillsPageModule)
  },
  {
    path: 'dismissal',
    loadChildren: () => import('./dismissal/dismissal.module').then( m => m.DismissalPageModule)
  },
  {
    path: 'transfer',
    loadChildren: () => import('./transfer/transfer.module').then( m => m.TransferPageModule)
  },
  {
    path: 'temporary-authorization',
    loadChildren: () => import('./temporary-authorization/temporary-authorization.module').then( m => m.TemporaryAuthorizationPageModule)
  },
  {
    path: 'eir-scholarships',
    loadChildren: () => import('./eir-scholarships/eir-scholarships.module').then( m => m.EirScholarshipsPageModule)
  },
  {
    path: 'exams',
    loadChildren: () => import('./exams/exams.module').then( m => m.ExamsPageModule)
  },
  {
    path: 'communications',
    loadChildren: () => import('./communications/communications.module').then( m => m.CommunicationsPageModule)
  },
  {
    path: 'social-benefit',
    loadChildren: () => import('./social-benefit/social-benefit.module').then( m => m.SocialBenefitPageModule)
  },
  {
    path: 'circulars',
    loadChildren: () => import('./circulars/circulars.module').then( m => m.CircularsPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
