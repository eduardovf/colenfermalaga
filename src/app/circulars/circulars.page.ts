import { Component, OnInit } from '@angular/core';
import { AlertController, LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-circulars',
  templateUrl: './circulars.page.html',
  styleUrls: ['./circulars.page.scss'],
})
export class CircularsPage implements OnInit {

  isLoading: boolean;
  circulars: any;

  constructor(
    private loadingCtrl: LoadingController,
    private alertController: AlertController
  ) { }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.isLoading = true;
    this.loadingCtrl
      .create({keyboardClose: true, message: 'Cargando...'})
      .then(loadingEl => {
        loadingEl.present();
        setTimeout(() => {
          this.circulars = [
            {
              id: 1,
              title: 'Convocatoria Junta Extraordinaria 2019',
              date: '2019-02-01',
              image: 'https://picsum.photos/200/300'
            },
            {
              id: 2,
              title: 'Convocatoria Junta Extraordinaria 2020',
              date: '2020-02-01',
              image: ''
            },
            {
              id: 3,
              title: 'Convocatoria Junta Extraordinaria 2021',
              date: '2021-02-01',
              image: ''
            },
          ];
          this.isLoading = false;
          loadingEl.dismiss();
        }, 1500);
      });
  }
}
