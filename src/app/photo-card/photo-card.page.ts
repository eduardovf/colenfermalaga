import { LoadingController, ToastController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-photo-card',
  templateUrl: './photo-card.page.html',
  styleUrls: ['./photo-card.page.scss'],
})
export class PhotoCardPage implements OnInit {

  image: string;
  isLoading: boolean;
  collegiate: any;

  constructor(
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController
  ) { }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.isLoading = true;
    this.loadingCtrl
      .create({keyboardClose: true, message: 'Cargando...'})
      .then(loadingEl => {
        loadingEl.present();
        setTimeout(() => {
          this.collegiate = {
            image: './../../assets/image_profile.jpg'
          };
          this.isLoading = false;
          loadingEl.dismiss();
        }, 1500);
      });
  }

  submit() {
    this.loadingCtrl
      .create({keyboardClose: true, message: 'Enviando...'})
      .then(loadingEl => {
        loadingEl.present();
        setTimeout(() => {
          this.toastCtrl
            .create({
              message: '<b>La solicitud ha sido enviada.</b><br>Recuerde que estos cambios serán revisados por el Colegio antes de hacerse efectivos',
              duration: 3000,
              cssClass: 'my-success-toast',
              color: 'success'
            })
            .then(toastEl => {
              toastEl.present();
            });
          loadingEl.dismiss();
        }, 1500);
      });
  }

  onImagePicked(imageData: string) {
    this.image = imageData;
  }
}
