import { SharedModule } from './../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PhotoCardPageRoutingModule } from './photo-card-routing.module';

import { PhotoCardPage } from './photo-card.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    PhotoCardPageRoutingModule
  ],
  declarations: [PhotoCardPage]
})
export class PhotoCardPageModule {}
