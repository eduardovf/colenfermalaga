import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PhotoCardPage } from './photo-card.page';

const routes: Routes = [
  {
    path: '',
    component: PhotoCardPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PhotoCardPageRoutingModule {}
