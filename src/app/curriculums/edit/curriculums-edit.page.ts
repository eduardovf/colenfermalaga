import { LoadingController, ToastController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Chooser } from '@awesome-cordova-plugins/chooser/ngx';

@Component({
  selector: 'app-curriculums-edit',
  templateUrl: './curriculums-edit.page.html',
  styleUrls: ['./curriculums-edit.page.scss'],
  providers: [ Chooser ]
})
export class CurriculumsEditPage implements OnInit {

  cvId: any;
  isLoading: boolean;
  cv: any;

  constructor(
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private route: ActivatedRoute,
    private chooser: Chooser) { }

  ngOnInit() {
    this.route.paramMap.subscribe(paramMap => {
      this.cvId = paramMap.has('id') || null;
      if(this.cvId) {
        this.getData(this.cvId);
      }
    });
  }

  getData(cvId) {
    if(cvId) {
      this.isLoading = true;
      this.loadingCtrl
        .create({keyboardClose: true, message: 'Cargando...'})
        .then(loadingEl => {
          loadingEl.present();
          setTimeout(() => {
            this.cv = {
              id: 1,
              title: 'Currículum técnico',
              observations: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas at purus mattis, ornare magna et, molestie ante. Nunc sit amet vestibulum ex. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas at purus mattis, ornare magna et, molestie ante. Nunc sit amet vestibulum ex.',
              file: 'ruta-al-cv.pdf',
              pending: false
            },
            this.isLoading = false;
            loadingEl.dismiss();
          }, 1500);
        });
      }
  }

  submit() {
    this.loadingCtrl
      .create({keyboardClose: true, message: 'Enviando...'})
      .then(loadingEl => {
        loadingEl.present();
        setTimeout(() => {
          this.toastCtrl
            .create({
              message: '<b>La solicitud ha sido enviada.</b><br>Recuerde que estos cambios serán revisados por el Colegio antes de hacerse efectivos',
              duration: 3000,
              cssClass: 'my-success-toast',
              color: 'success'
            })
            .then(toastEl => {
              toastEl.present();
            });
          loadingEl.dismiss();
        }, 1500);
      });
  }

  upload() {
    this.chooser.getFile()
    .then(file => {
      console.log(file ? file.name : 'canceled');
    })
    .catch((error: any) => console.error(error));
  }
}
