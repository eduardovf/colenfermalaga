import { AlertController, LoadingController, ToastController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-curriculums-list',
  templateUrl: './curriculums-list.page.html',
  styleUrls: ['./curriculums-list.page.scss'],
})
export class CurriculumsListPage implements OnInit {

  isLoading: boolean;
  cv: any;

  constructor(
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private alertController: AlertController
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.getData();
  }

  getData() {
    this.isLoading = true;
    this.loadingCtrl
      .create({keyboardClose: true, message: 'Cargando...'})
      .then(loadingEl => {
        loadingEl.present();
        setTimeout(() => {
          this.cv = [
            {
              id: 1,
              title: 'Currículum técnico',
              observations: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas at purus mattis, ornare magna et, molestie ante. Nunc sit amet vestibulum ex. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas at purus mattis, ornare magna et, molestie ante. Nunc sit amet vestibulum ex.',
              file: 'ruta-al-cv.pdf',
              pending: false
            },
            {
              id: 2,
              title: 'Currículum reducido',
              observations: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
              file: 'ruta-al-cv-abreviad.pdf',
              pending: true
            },
          ];
          this.isLoading = false;
          loadingEl.dismiss();
        }, 1500);
      });
  }

  deleteCV(cvId) {
    this.alertController
      .create({
        header: '¿Estás seguro?',
        message: 'Has solicitado eliminar un elemento. Confirma que  es eso lo que quieres hacer.',
        buttons: [
          {
            text: 'Cancelar',
            role: 'cancel',
            cssClass: 'secondary'
          },
          {
            text: 'OK',
            cssClass: 'primary',
            handler: () => {
              this.loadingCtrl
                .create({keyboardClose: true, message: 'Enviando...'})
                .then(loadingEl => {
                  loadingEl.present();
                  setTimeout(() => {
                    this.toastCtrl
                      .create({
                        message: '<b>La solicitud ha sido enviada.</b><br>Recuerde que estos cambios serán revisados por el Colegio antes de hacerse efectivos',
                        duration: 3000,
                        cssClass: 'my-success-toast',
                        color: 'success'
                      })
                      .then(toastEl => {
                        toastEl.present();
                      });
                    loadingEl.dismiss();
                    this.getData();
                  }, 1500);
                });
            }
          }
        ]
      })
      .then(alertEl => {
        alertEl.present();
      });
  }
}
