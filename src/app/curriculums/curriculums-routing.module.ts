import { CurriculumsEditPage } from './edit/curriculums-edit.page';
import { CurriculumsListPage } from './curriculums-list.page';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', component: CurriculumsListPage },
  { path: 'new', component: CurriculumsEditPage },
  { path: 'edit', component: CurriculumsEditPage },
  { path: 'edit/:id', component: CurriculumsEditPage }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CurriculumsRoutingModule {}
