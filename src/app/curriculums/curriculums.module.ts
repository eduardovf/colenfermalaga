import { SharedModule } from './../shared/shared.module';
import { CurriculumsEditPage } from './edit/curriculums-edit.page';
import { CurriculumsListPage } from './curriculums-list.page';
import { CurriculumsRoutingModule } from './curriculums-routing.module';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    CurriculumsRoutingModule
  ],
  declarations: [
    CurriculumsListPage,
    CurriculumsEditPage,
  ],
})
export class CurriculumsModule { }
