import { AddressesListPage } from './addresses-list.page';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddressesPageRoutingModule } from './addresses-routing.module';
import { AddressesEditPage } from './edit/addresses-edit.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddressesPageRoutingModule,
  ],
  declarations: [
    AddressesListPage,
    AddressesEditPage
  ]
})
export class AddressesPageModule {}
