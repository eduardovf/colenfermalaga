import { LoadingController, ToastController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-addresses-edit',
  templateUrl: './addresses-edit.page.html',
  styleUrls: ['./addresses-edit.page.scss'],
})
export class AddressesEditPage implements OnInit {

  addressId: any;
  isLoading: boolean;
  address: any;

  constructor(
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.paramMap.subscribe(paramMap => {
      this.addressId = paramMap.has('id') || null;
      if(this.addressId) {
        this.getData(this.addressId);
      }
    });
  }

  getData(addressId) {
    if(addressId) {
      this.isLoading = true;
      this.loadingCtrl
        .create({keyboardClose: true, message: 'Cargando...'})
        .then(loadingEl => {
          loadingEl.present();
          setTimeout(() => {
            this.address = {
                id: 2,
                title: 'Casa',
                main: false,
                address: 'URB. BELLO HORIZONTE 3, FASE 1, CASA 5',
                address2: 'Junto al banco',
                cp: '29603',
                localty: 'Marbella',
                province: 'Málaga',
                movil: null,
                phone: '555111222',
                pending: false
              };
            this.isLoading = false;
            loadingEl.dismiss();
          }, 1500);
        });
      }
  }

  submit() {
    this.loadingCtrl
      .create({keyboardClose: true, message: 'Enviando...'})
      .then(loadingEl => {
        loadingEl.present();
        setTimeout(() => {
          this.toastCtrl
            .create({
              message: '<b>La solicitud ha sido enviada.</b><br>Recuerde que estos cambios serán revisados por el Colegio antes de hacerse efectivos',
              duration: 3000,
              cssClass: 'my-success-toast',
              color: 'success'
            })
            .then(toastEl => {
              toastEl.present();
            });
          loadingEl.dismiss();
        }, 1500);
      });
  }
}
