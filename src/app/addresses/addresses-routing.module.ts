import { AddressesListPage } from './addresses-list.page';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddressesEditPage } from './edit/addresses-edit.page';

const routes: Routes = [
  { path: '', component: AddressesListPage },
  { path: 'new', component: AddressesEditPage },
  { path: 'edit', component: AddressesEditPage },
  { path: 'edit/:id', component: AddressesEditPage }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddressesPageRoutingModule {}
