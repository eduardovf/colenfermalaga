import { AlertController, LoadingController, ToastController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-addresses-list',
  templateUrl: './addresses-list.page.html',
  styleUrls: ['./addresses-list.page.scss'],
})
export class AddressesListPage implements OnInit {

  isLoading: boolean;
  addresses: any;

  constructor(
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private alertController: AlertController
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.getData();
  }

  getData() {
    this.isLoading = true;
    this.loadingCtrl
      .create({keyboardClose: true, message: 'Cargando...'})
      .then(loadingEl => {
        loadingEl.present();
        setTimeout(() => {
          this.addresses = [
            {
              id: 1,
              title: 'Trabajo',
              main: true,
              address: 'URB. BELLO HORIZONTE 3, FASE 1, CASA 5',
              cp: '29603',
              localty: 'Marbella',
              province: 'Málaga',
              movil: '61234567',
              phone: '555111222',
              pending: false
            },
            {
              id: 2,
              title: 'Casa',
              main: false,
              address: 'URB. BELLO HORIZONTE 3, FASE 1, CASA 5',
              address2: 'Junto al banco',
              cp: '29603',
              localty: 'Marbella',
              province: 'Málaga',
              phone: '555111222',
              pending: false
            },
            {
              id: 3,
              title: 'Gimnasio',
              main: false,
              address: 'URB. BELLO HORIZONTE 3, FASE 1, CASA 5',
              cp: '29603',
              localty: 'Marbella',
              province: 'Málaga',
              pending: true
            }
          ];
          this.isLoading = false;
          loadingEl.dismiss();
        }, 1500);
      });
  }

  deleteAddress(addressId) {
    this.alertController
      .create({
        header: '¿Estás seguro?',
        message: 'Has solicitado eliminar un elemento. Confirma que  es eso lo que quieres hacer.',
        buttons: [
          {
            text: 'Cancelar',
            role: 'cancel',
            cssClass: 'secondary'
          },
          {
            text: 'OK',
            cssClass: 'primary',
            handler: () => {
              this.loadingCtrl
                .create({keyboardClose: true, message: 'Enviando...'})
                .then(loadingEl => {
                  loadingEl.present();
                  setTimeout(() => {
                    this.toastCtrl
                      .create({
                        message: '<b>La solicitud ha sido enviada.</b><br>Recuerde que estos cambios serán revisados por el Colegio antes de hacerse efectivos',
                        duration: 3000,
                        cssClass: 'my-success-toast',
                        color: 'success'
                      })
                      .then(toastEl => {
                        toastEl.present();
                      });
                    loadingEl.dismiss();
                    this.getData();
                  }, 1500);
                });
            }
          }
        ]
      })
      .then(alertEl => {
        alertEl.present();
      });
  }
}
