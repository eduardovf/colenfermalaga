import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProfessionalDataPageRoutingModule } from './professional-data-routing.module';

import { ProfessionalDataPage } from './professional-data.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProfessionalDataPageRoutingModule
  ],
  declarations: [ProfessionalDataPage]
})
export class ProfessionalDataPageModule {}
