import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CurrentInscriptionsPage } from './current-inscriptions.page';

const routes: Routes = [
  {
    path: '',
    component: CurrentInscriptionsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CurrentInscriptionsPageRoutingModule {}
