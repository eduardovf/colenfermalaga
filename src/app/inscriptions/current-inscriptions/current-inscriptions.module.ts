import { SharedModule } from './../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CurrentInscriptionsPageRoutingModule } from './current-inscriptions-routing.module';

import { CurrentInscriptionsPage } from './current-inscriptions.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    CurrentInscriptionsPageRoutingModule
  ],
  declarations: [CurrentInscriptionsPage],
})
export class CurrentInscriptionsPageModule {}
