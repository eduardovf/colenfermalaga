import { LoadingController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-current-inscriptions',
  templateUrl: './current-inscriptions.page.html',
  styleUrls: ['./current-inscriptions.page.scss'],
})
export class CurrentInscriptionsPage implements OnInit {

  isLoading: boolean;
  inscriptions: any;

  constructor(private loadingCtrl: LoadingController) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.getData();
  }

  getData() {
    this.isLoading = true;
    this.loadingCtrl
      .create({keyboardClose: true, message: 'Cargando...'})
      .then(loadingEl => {
        loadingEl.present();
        setTimeout(() => {
          this.inscriptions = [
            {
              id: 1,
              title: 'Valoración de la Disfagia tras Ictus 2 (segunda edición)',
              reference: 'CR210000162',
              date: '2020-02-01',
              image: 'https://picsum.photos/200/300',
            },
            {
              id: 2,
              title: 'Valoración de la Disfagia tras Ictus 2 (segunda edición)',
              reference: 'CR210000163',
              date: '2020-02-01',
              image: 'https://picsum.photos/300/300',
            },
            {
              id: 3,
              title: 'Valoración de la Disfagia tras Ictus 2 (segunda edición)',
              reference: 'CR210000164',
              date: '2020-02-01',
              image: 'https://picsum.photos/400/300',
            },
          ];
          this.isLoading = false;
          loadingEl.dismiss();
        }, 1500);
      });
  }
}
