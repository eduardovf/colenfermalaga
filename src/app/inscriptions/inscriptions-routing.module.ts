import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InscriptionsPage } from './inscriptions.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: InscriptionsPage,
    children: [
      {
        path: 'current',
        children: [
          {
            path: '',
            loadChildren: () => import('./current-inscriptions/current-inscriptions.module').then( m => m.CurrentInscriptionsPageModule)
          },
          {
            path: ':inscriptionId',
            loadChildren: () => import('./inscription-detail/inscription-detail.module').then( m => m.InscriptionDetailPageModule),
          },
        ]
      },
      {
        path: 'historic',
        children: [
          {
            path: '',
            loadChildren: () => import('./historic-inscriptions/historic-inscriptions.module').then( m => m.HistoricInscriptionsPageModule)
          },
        ]
      },
      {
        path: '',
        redirectTo: '/tabs/current',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/inscriptions/tabs/current',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InscriptionsPageRoutingModule {}
