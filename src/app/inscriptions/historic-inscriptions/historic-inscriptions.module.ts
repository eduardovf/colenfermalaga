import { SharedModule } from './../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HistoricInscriptionsPageRoutingModule } from './historic-inscriptions-routing.module';

import { HistoricInscriptionsPage } from './historic-inscriptions.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    HistoricInscriptionsPageRoutingModule
  ],
  declarations: [HistoricInscriptionsPage]
})
export class HistoricInscriptionsPageModule {}
