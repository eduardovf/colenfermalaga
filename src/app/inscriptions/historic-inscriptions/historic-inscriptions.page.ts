import { LoadingController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-historic-inscriptions',
  templateUrl: './historic-inscriptions.page.html',
  styleUrls: ['./historic-inscriptions.page.scss'],
})
export class HistoricInscriptionsPage implements OnInit {

  isLoading: boolean;
  inscriptions: any;

  constructor(private loadingCtrl: LoadingController) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.getData();
  }

  getData() {
    this.isLoading = true;
    this.loadingCtrl
      .create({keyboardClose: true, message: 'Cargando...'})
      .then(loadingEl => {
        loadingEl.present();
        setTimeout(() => {
          this.inscriptions = [
            // {
            //   id: 4,
            //   title: 'CURSOS EXPERTOS UNIVERSITARIOS: Cuidados al paciente Crónico y gestión de casos de enfermería',
            //   reference: 'CR210000162',
            //   date: '2020-02-01',
            //   image: 'https://picsum.photos/200/300',
            // }
          ];
          this.isLoading = false;
          loadingEl.dismiss();
        }, 1500);
      });
  }
}
