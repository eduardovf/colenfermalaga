import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HistoricInscriptionsPage } from './historic-inscriptions.page';

const routes: Routes = [
  {
    path: '',
    component: HistoricInscriptionsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HistoricInscriptionsPageRoutingModule {}
