import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InscriptionDetailPageRoutingModule } from './inscription-detail-routing.module';

import { InscriptionDetailPage } from './inscription-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InscriptionDetailPageRoutingModule
  ],
  declarations: [InscriptionDetailPage]
})
export class InscriptionDetailPageModule {}
