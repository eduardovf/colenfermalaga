import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InscriptionDetailPage } from './inscription-detail.page';

const routes: Routes = [
  {
    path: '',
    component: InscriptionDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InscriptionDetailPageRoutingModule {}
