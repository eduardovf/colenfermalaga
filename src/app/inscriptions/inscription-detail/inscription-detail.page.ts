import { LoadingController } from '@ionic/angular';
import { Component, ElementRef, OnInit, Renderer2, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-inscription-detail',
  templateUrl: './inscription-detail.page.html',
  styleUrls: ['./inscription-detail.page.scss'],
})
export class InscriptionDetailPage implements OnInit {

  @ViewChild('map', { static: false }) mapElementRef: ElementRef;
  googleMaps: any;

  inscriptionId: any;
  isLoading: boolean;
  inscription: any;

  constructor(
    private loadingCtrl: LoadingController,
    private route: ActivatedRoute,
    private renderer: Renderer2
  ) { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.route.paramMap.subscribe(paramMap => {
      this.inscriptionId = paramMap.has('inscriptionId') || null;
      if(this.inscriptionId) {
        this.getData(this.inscriptionId);
      }
    });
  }

  getData(inscriptionId) {
    if(inscriptionId) {
      this.isLoading = true;
      this.loadingCtrl
        .create({keyboardClose: true, message: 'Cargando...'})
        .then(loadingEl => {
          loadingEl.present();
          setTimeout(() => {
            this.inscription = {
              id: 1,
              general: {
                date: '2020-02-01',
                status: 'Inscrito',
                collegiate: '00001',
                name: 'Michael Terence Smith',
                documentNumber: '12345798',
                address: 'URB. BELLO HORIZONTE 3, FASE 1, CASA 5 29603 MARBELLA MÁLAGA',
                email: 'email@email.com',
                phone: '555123456'
              },
              payments: {
                dateLimit: '2020-02-01',
                datePaid: '2020-02-01',
                status: 'Pagado',
              },
              course: {
                title: 'Valoración de la Disfagia tras Ictus 2 (segunda edición)',
                reference: 'CR20000162',
                category: 'Curso online',
                modality: 'Online',
                dateStart: '2020-02-01',
                address: 'Dirección del curso',
                lat: 37.38236699411998,
                lng: -5.973852744173977,
                url: 'https://colenfermalaga.com/cursos/curso/207/valoracion-de-la-disfagia-tras-ictus-2-segunda-edicion'
              }
            };
            this.isLoading = false;
            loadingEl.dismiss();

            // Pintar mapa
            this.getGoogleMaps()
              .then(googleMaps => {
                this.googleMaps = googleMaps;
                const mapEl = this.mapElementRef.nativeElement;
                const map = new googleMaps.Map(mapEl, {
                  center: { lat: this.inscription.course.lat, lng: this.inscription.course.lng },
                  zoom: 16
                });

                this.googleMaps.event.addListenerOnce(map, 'idle', () => {
                  this.renderer.addClass(mapEl, 'visible');
                });

                const marker = new googleMaps.Marker({
                  position: { lat: this.inscription.course.lat, lng: this.inscription.course.lng },
                  map: map
                });
                marker.setMap(map);
              })
              .catch(err => {
                console.log(err);
              });
          }, 1500);
        });
      }
  }

  private getGoogleMaps(): Promise<any> {
    const win = window as any;
    const googleModule = win.google;
    if (googleModule && googleModule.maps) {
      return Promise.resolve(googleModule.maps);
    }
    return new Promise((resolve, reject) => {
      const script = document.createElement('script');
      script.src =
        'https://maps.googleapis.com/maps/api/js?key=' +
        environment.googleMapsAPIKey;
      script.async = true;
      script.defer = true;
      document.body.appendChild(script);
      script.onload = () => {
        const loadedGoogleModule = win.google;
        if (loadedGoogleModule && loadedGoogleModule.maps) {
          resolve(loadedGoogleModule.maps);
        } else {
          reject('Google maps SDK not available.');
        }
      };
    });
  }
}
