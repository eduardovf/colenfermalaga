import { LanguagesEditPage } from './edit/languages-edit.page';
import { LanguagesListPage } from './languages-list.page';
import { LanguagesRoutingModule } from './languages-routing.module';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LanguagesRoutingModule
  ],
  declarations: [
    LanguagesListPage,
    LanguagesEditPage
  ],
})
export class LanguagesModule { }
