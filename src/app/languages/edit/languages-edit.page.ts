import { LoadingController, ToastController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-languages-edit',
  templateUrl: './languages-edit.page.html',
  styleUrls: ['./languages-edit.page.scss'],
})
export class LanguagesEditPage implements OnInit {

  languageId: any;
  isLoading: boolean;
  language: any;

  constructor(
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.paramMap.subscribe(paramMap => {
      this.languageId = paramMap.has('id') || null;
      if(this.languageId) {
        this.getData(this.languageId);
      }
    });
  }

  getData(languageId) {
    if(languageId) {
      this.isLoading = true;
      this.loadingCtrl
        .create({keyboardClose: true, message: 'Cargando...'})
        .then(loadingEl => {
          loadingEl.present();
          setTimeout(() => {
            this.language = {
              id: 1,
              title: 'INGLÉS',
              level: 'C2',
              date: '2020-02-01',
            },
            this.isLoading = false;
            loadingEl.dismiss();
          }, 1500);
        });
      }
  }

  submit() {
    this.loadingCtrl
      .create({keyboardClose: true, message: 'Enviando...'})
      .then(loadingEl => {
        loadingEl.present();
        setTimeout(() => {
          this.toastCtrl
            .create({
              message: '<b>La solicitud ha sido enviada.</b><br>Recuerde que estos cambios serán revisados por el Colegio antes de hacerse efectivos',
              duration: 3000,
              cssClass: 'my-success-toast',
              color: 'success'
            })
            .then(toastEl => {
              toastEl.present();
            });
          loadingEl.dismiss();
        }, 1500);
      });
  }
}
