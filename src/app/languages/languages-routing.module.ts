import { LanguagesEditPage } from './edit/languages-edit.page';
import { LanguagesListPage } from './languages-list.page';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', component: LanguagesListPage },
  { path: 'new', component: LanguagesEditPage },
  { path: 'edit', component: LanguagesEditPage },
  { path: 'edit/:id', component: LanguagesEditPage }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LanguagesRoutingModule {}
