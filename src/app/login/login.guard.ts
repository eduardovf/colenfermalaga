import { LoginService } from './login.service';
import { Injectable } from '@angular/core';
import { CanLoad, UrlTree, Router, UrlSegment } from '@angular/router';
import { Observable } from 'rxjs';
import { Route } from '@angular/compiler/src/core';

@Injectable({
  providedIn: 'root'
})
export class LoginGuard implements CanLoad {

  constructor(
    private loginService: LoginService,
    private router: Router) {}

  canLoad(
    route: Route,
    segments: UrlSegment[]): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      if(!this.loginService.userIsLogged) {
        this.router.navigateByUrl('/login');
      }
      return this.loginService.userIsLogged;
    }

}
