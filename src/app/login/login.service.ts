import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private _logged = true;

  constructor() { }

  get userIsLogged() {
    return this._logged;
  }

  login() {
    this._logged = true;
  }

  logout() {
    this._logged = false;
  }
}
