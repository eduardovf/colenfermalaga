import { LoadingController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-bills',
  templateUrl: './bills.page.html',
  styleUrls: ['./bills.page.scss'],
})
export class BillsPage implements OnInit {

  isLoading: boolean;
  bills: any;

  constructor(private loadingCtrl: LoadingController) { }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.isLoading = true;
    this.loadingCtrl
      .create({keyboardClose: true, message: 'Cargando...'})
      .then(loadingEl => {
        loadingEl.present();
        setTimeout(() => {
          this.bills = [
            {
              id: 1,
              title: 'FACTURA 01',
              date: '2021-02-01',
              amount: 999.99
            },
            {
              id: 2,
              title: 'FACTURA 02',
              date: '2021-02-01',
              amount: 999.99
            },
            {
              id: 3,
              title: 'FACTURA 03',
              date: '2021-02-01',
              amount: 999.99
            },
          ];
          this.isLoading = false;
          loadingEl.dismiss();
        }, 1500);
      });
  }
}
