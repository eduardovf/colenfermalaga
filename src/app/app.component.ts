import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {

  menu: any;

  constructor() {
    this.sideMenu();
  }

  sideMenu() {
    this.menu =
    [
      {
        title: 'Personal',
        items: [
          {
            title: 'Datos personales y económicos',
            icon: 'person-outline',
            url: '/personal-data'
          },
          {
            title: 'Configuración web',
            icon: 'earth',
            url: '/web-configuration'
          },
          {
            title: 'Foto carnet',
            icon: 'id-card-outline',
            url: '/photo-card'
          },
          {
            title: 'Direcciones',
            icon: 'location-outline',
            url: '/addresses'
          },
        ]
      },
      {
        title: 'Profesional',
        items: [
          {
            title: 'Datos profesionales',
            icon: 'briefcase-outline',
            url: '/professional-data'
          },
          {
            title: 'Empresas',
            icon: 'business-outline',
            url: '/business'
          },
          {
            title: 'Currículums',
            icon: 'document-text-outline',
            url: '/curriculums'
          },
        ]
      },
      {
        title: 'Académico',
        items: [
          {
            title: 'Datos académicos',
            icon: 'school-outline',
            url: '/academics'
          },
           {
            title: 'Especialidades',
            icon: 'star-outline',
            url: '/specialties'
          },
           {
            title: 'Idiomas',
            icon: 'language-outline',
            url: '/languages'
          },
        ]
      },
      {
        title: 'Varios',
        items: [
          {
            title: 'Inscripciones',
            icon: 'clipboard-outline',
            url: '/inscriptions'
          },
          {
            title: 'Certificados',
            icon: 'ribbon-outline',
            url: '/certificates'
          },
          {
            title: 'Habilitaciones temporales',
            icon: 'time-outline',
            url: '/temporal-habilitation'
          },
          {
            title: 'Facturas',
            icon: 'receipt-outline',
            url: '/bills'
          },
        ]
      },
      {
        title: 'Ventanilla única',
        items: [
          {
            title: 'Baja colegial',
            icon: 'person-remove-outline',
            url: '/dismissal'
          },
          {
            title: 'Traslado colegial',
            icon: 'swap-horizontal-outline',
            url: '/transfer'
          },
          {
            title: 'Solicitud habilitación temporal',
            icon: 'time-outline',
            url: '/temporary-authorization'
          },
          {
            title: 'Solicitud becas EIR',
            icon: 'cash-outline',
            url: '/eir-scholarships'
          },
          {
            title: 'Ayuda preparación exámenes',
            icon: 'easel-outline',
            url: '/exams'
          },
          {
            title: 'Ayuda ponencias/comunicaciones',
            icon: 'headset-outline',
            url: '/communications'
          },
          {
            title: 'Prestación social',
            icon: 'hand-right-outline',
            url: '/social-benefit'
          },
          {
            title: 'Circulares',
            icon: 'documents-outline',
            url: '/circulars'
          },
        ]
      },
    ];
  }

}
