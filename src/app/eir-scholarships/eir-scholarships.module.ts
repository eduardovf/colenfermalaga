import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EirScholarshipsPageRoutingModule } from './eir-scholarships-routing.module';

import { EirScholarshipsPage } from './eir-scholarships.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EirScholarshipsPageRoutingModule
  ],
  declarations: [EirScholarshipsPage]
})
export class EirScholarshipsPageModule {}
