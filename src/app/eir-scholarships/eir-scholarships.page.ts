import { Component, OnInit } from '@angular/core';
import { LoadingController, ToastController } from '@ionic/angular';
import { Chooser } from '@awesome-cordova-plugins/chooser/ngx';

@Component({
  selector: 'app-eir-scholarships',
  templateUrl: './eir-scholarships.page.html',
  styleUrls: ['./eir-scholarships.page.scss'],
  providers: [ Chooser ]
})
export class EirScholarshipsPage implements OnInit {

  isLoading: boolean;
  collegiate: any;

  constructor(
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private chooser: Chooser) { }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.isLoading = true;
    this.loadingCtrl
      .create({keyboardClose: true, message: 'Cargando...'})
      .then(loadingEl => {
        loadingEl.present();
        setTimeout(() => {
          this.collegiate = {
            phone: '555123456',
            email: 'michael.terence.smith@midominio.com'
          };
          this.isLoading = false;
          loadingEl.dismiss();
        }, 1500);
      });
  }

  submit() {
    this.loadingCtrl
      .create({keyboardClose: true, message: 'Enviando...'})
      .then(loadingEl => {
        loadingEl.present();
        setTimeout(() => {
          this.toastCtrl
            .create({
              message: '<b>La solicitud ha sido enviada.</b><br>Recuerde que estos cambios serán revisados por el Colegio antes de hacerse efectivos',
              duration: 3000,
              cssClass: 'my-success-toast',
              color: 'success'
            })
            .then(toastEl => {
              toastEl.present();
            });
          loadingEl.dismiss();
        }, 1500);
      });
  }

  upload() {
    this.chooser.getFile()
    .then(file => {
      console.log(file ? file.name : 'canceled');
    })
    .catch((error: any) => console.error(error));
  }
}
