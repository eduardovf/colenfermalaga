import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EirScholarshipsPage } from './eir-scholarships.page';

const routes: Routes = [
  {
    path: '',
    component: EirScholarshipsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EirScholarshipsPageRoutingModule {}
